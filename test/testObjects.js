const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions

const object = require("./objects.js");

const keys = object.keys(testObject);
console.log(keys);

const values = object.values(testObject);
console.log(values);

const mapObject = object.mapObject(testObject, (a) => a);
console.log(mapObject);

const pairs = object.pairs(testObject);
console.log(pairs);

const invert = object.invert(testObject);
console.log(invert);

const invert1 = object.invert(invert);
console.log(invert1);

const iceCream = { flavor: "chocolate" };

const defaults = object.defaults(iceCream, { flavor: "vanilla", sprinkles: "lots" });
console.log(defaults);