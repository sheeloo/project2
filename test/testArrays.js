const array = require("../arrays.js");

const items = [1, 2, 3, 4, 5, 5]; // use this array to test your code.

array.each(items, (element) => { console.log(`element = ${element}`) });

array.each(items, (element, index) => { console.log(`element = ${element}, index = ${index}`) });


let map1 = array.map(items, (element) => { return element });
console.log(map1);

let map2 = array.map(items, (element, index) => { return element * index });
console.log(map2);


let reduce1 = array.reduce(items, (accumulator, currentValue) => { return accumulator + currentValue });
console.log(reduce1);

let reduce2 = array.reduce(["a", "b", "c"], (accumulator, currentValue) => { return accumulator + currentValue }, [1, 2]);
console.log(reduce2);


let find1 = array.find(items, (element, index) => { return element < 4 });
console.log(find1);


let filter1 = array.filter(items, (element) => { return element < 4 });
console.log(filter1);

const nestedArray = [1, [2],
    [
        [3]
    ],
    [
        [
            [4]
        ]
    ]
]; // use this to test 'flatten'

const depth = 4;

let flatten1 = array.flatten(nestedArray, depth);

console.log(flatten1);