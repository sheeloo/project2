const closure = require("./closures.js");

const counter = Object.create(closure.counterFactory());

console.log(counter.increment());
console.log(counter.decrement());

const func = function() {

    const arg = Object.values(arguments);

    console.log(...arg)
}


const res = closure.limitFunctionCallCount(func, 2);

res("Hi..");
res("Hi..");
res("Hi..");
res("Hi..");

const cb = function() {

    const arg = Object.values(arguments);

    return [...arg];
}

const test = closure.cacheFunction(cb);

console.log(test(2));
console.log(test(5, 2));
console.log(test(5, 2));
console.log(test("hi.", 1, 2, 3));