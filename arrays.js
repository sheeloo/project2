const arrays = {}

arrays.each = function(elements, cb) {

    for (let index = 0; index < elements.length; index++) {

        cb(elements[index], index, elements);

    }
}

arrays.map = function(elements, cb) {

    let arr = [];

    for (let index = 0; index < elements.length; index++) {

        let element = cb(elements[index], index, elements);

        arr.push(element);

    }

    return arr;

}

arrays.reduce = function(elements, cb, startingValue) {

    let startingIndex = 0;

    if (startingValue == undefined) {

        startingValue = elements[startingIndex];

        startingIndex = 1;

    }

    for (let index = startingIndex; index < elements.length; index++) {

        startingValue = cb(startingValue, elements[index], index, elements);

    }
    return startingValue;

}

arrays.find = function(elements, cb) {

    let found_element;

    if (elements == undefined || !Array.isArray(elements) || cb == undefined) {
        return undefined;
    }

    for (let index = 0; index < elements.length; index++) {

        let isElementFound = cb(elements[index], index, elements);

        if (isElementFound) {

            found_element = elements[index];

            break;
        }

    }

    return found_element;

}

arrays.filter = function(elements, cb) {

    let found_elements = [];
    if (elements == undefined || !Array.isArray(elements) || cb == undefined) {

        return [];
    }

    for (let index = 0; index < elements.length; index++) {

        let isElementFound = cb(elements[index], index, elements);

        if (isElementFound) {

            found_elements.push(elements[index]);

        }

    }

    return found_elements;
}


const flatArray = [];

arrays.flatten = function(elements, depth = Infinity) {

    if (depth > 0) {

        for (let index = 0, length = elements.length; index < length; index++) {

            const element = elements[index];

            if (Array.isArray(element)) {

                this.flatten(element, depth - 1);

            } else {

                flatArray.push(element);
            }

        }
        return flatArray;
    }

}





module.exports = arrays;