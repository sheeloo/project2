const objects = {};

objects.keys = function(obj) {

    let keys = [];

    for (let key in obj) {

        keys.push(key);

    }

    return keys;

}

objects.values = function(obj) {

    let values = [];

    for (let key in obj) {

        values.push(obj[key]);

    }

    return values;

}

objects.mapObject = function(obj, cb) {

    let newObj = {};

    for (let key in obj) {

        newObj[key] = cb(obj[key], key);

    }

    return newObj;

}


objects.pairs = function(obj) {

    let pair = [];

    for (let key in obj) {

        pair.push([key, obj[key]]);

    }

    return pair;

}

objects.invert = function(obj) {

    let invertObj = {};

    for (let key in obj) {

        invertObj[obj[key]] = key;

    }

    return invertObj;

}


objects.defaults = function(obj, defaultProps) {

    let invertObj = {...obj };

    for (let key in defaultProps) {

        if (invertObj[key] === undefined) {

            invertObj[key] = defaultProps[key];
        }

    }
    return invertObj;

}

module.exports = objects;