const closures = {};

closures.counterFactory = function() {

    let count = 0;
    let obj = {};

    obj.increment = function() {
        count++;
        return count;
    };
    obj.decrement = function() {
        count--;
        return count;
    };

    return obj;
}


closures.limitFunctionCallCount = function(cb, n) {

    let count = 0;

    return function() {

        const arg = Object.values(arguments);

        if (count < n) {

            count++;
            return cb(...arg);

        }
    }

}


closures.cacheFunction = function(cb) {

    let cache = {};

    return function() {

        const arg = Object.values(arguments);

        if (arg.toString() in cache) {

            console.log("from cache")

            return cache[arg]

        } else {

            console.log('Calculating result');

            const res = cb(...arg);

            return cache[arg] = res;
        }
    }

}


module.exports = closures;